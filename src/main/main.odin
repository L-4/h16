package main

import "core:fmt"

import "../h16/h16_vm"
import "../h16/h16_assembler"
import "../h16//h16_types"

Syscall_Op :: enum i16 {
	Write_Value,
	Was_Prime,
	Was_Not_Prime,
}

syscall :: proc(vm: ^h16_vm.H16, code, param: i16) {
	switch Syscall_Op(code) {
		case .Write_Value:   fmt.printf("H16 says: {}\n", param)
		case .Was_Prime:     fmt.println("prime:   ", param)
		case .Was_Not_Prime: fmt.println("primem't:", param)

		case: fmt.panicf("Unhandeled syscall: {} (param = {})\n", code, param)
	}
}

main :: proc() {
	// Generate data required for assembling from enums
	h16_types.generate_op_descriptors()

	// Returns builder which also contains info about label positions
	TEST_FILE :: "tests/fib.h16"
	// TEST_FILE :: "tests/primes.h16"
	bin, debug_info := h16_assembler.assemble_from_file(TEST_FILE)
	defer delete(bin)

	// Create vm from memory
	vm := h16_vm.h16_create(0xffff, bin, syscall, debug_info)
	defer h16_vm.h16_destroy(vm)

	// Run until halt is called
	h16_vm.h16_run(&vm)
}
