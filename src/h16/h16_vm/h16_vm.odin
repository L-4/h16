package h16_vm

// TODO:
// - Misc instructions
// - More robust assembler:
//		- Test file with weird syntax
//		- Rewrite parsing to support assemble-time arithmetic
// - Rename ops to lowercase?
// - Have heavy/light op descriptors
// - Pass to make things inline and contextless
// - Better procs in builder
// - Better debugging tools for eg stack
// - Load from binary
// - Allow stepping through
// - Addressing modes

// https://en.wikipedia.org/wiki/Register_allocation
// https://en.wikipedia.org/wiki/Static_single-assignment_form

import "core:mem"
import "core:fmt"
import "core:reflect"
import "core:intrinsics"

import "../h16_types"
import "../h16_binary_iface"

Register :: h16_types.Register
Address :: h16_types.Address
Op :: h16_types.Op
Register_Bits :: h16_types.Register_Bits

// @todo: Fix!
import "../h16_assembler"
Debug_Info :: h16_assembler.Debug_Info

__trap :: intrinsics.debug_trap

Syscall :: #type proc(vm: ^H16, code, param: i16)

H16 :: struct {
	registers: [Register]i16,
	memory: []u8,
	syscall: Syscall,
	debug_info: Maybe(Debug_Info),
	user_data: rawptr,
}

// Create/destroy
h16_create :: proc(
	memory_size: u16 = 0xffff,
	initial_data: h16_binary_iface.H16_Bytecode,
	syscall: Syscall = proc(vm: ^H16, code, param: i16) {},
	debug_info: Maybe(Debug_Info) = nil,
) -> H16 {
	vm: H16

	assert(memory_size >= 0 && memory_size <= 0xffff)
	assert(len(initial_data) < 0xffff && u16(len(initial_data)) <= memory_size)

	vm.memory = make([]u8, memory_size)
	mem.copy(raw_data(vm.memory), raw_data(initial_data), len(initial_data))
	set_register(&vm, .sp, Address(memory_size - 2))
	set_register(&vm, .bp, Address(memory_size - 2))

	vm.syscall = syscall
	vm.debug_info = debug_info

	return vm
}

h16_destroy :: proc(vm: H16) {
	delete(vm.memory)
	if di, ok := vm.debug_info.?; ok {
		h16_binary_iface.debug_info_free(&di)
	}
}

assert_register_writable :: #force_inline proc(register: Register) {
	if !(register in h16_types.writable_registers) {
		fmt.panicf("Tried to write to read only register '{}'!\n", register)
	}
}

get_register :: #force_inline proc(vm: H16, register: Register, $T: typeid) -> T {
	#assert(size_of(T) == 2)
	when ODIN_DEBUG { // @todo: Good idea?
		if register in (Register_Bits{ .pc, .sp, .bp }) {
			assert(T == Address, "Loading register address as something else!")
		}
	}

	return transmute(T)vm.registers[register]
}

set_register :: #force_inline proc(vm: ^H16, register: Register, val: $T) {
	#assert(size_of(T) == 2)
	when ODIN_DEBUG {
		if register in (Register_Bits{ .pc, .sp, .bp }) {
			assert(T == Address, "Storing register address as something else!")
		}
	}

	vm.registers[register] = transmute(i16)val
}

inc_register :: #force_inline proc(vm: ^H16, register: Register, val: i16) {
	vm.registers[register] += transmute(i16)val
}

validate_memory_range :: #force_inline proc (vm: H16, address, size: Address) {
	when ODIN_DEBUG {
		assert(address + size <= Address(len(vm.memory)), "Memory out of range!")
	}
}

load_8 :: #force_inline proc(vm: H16, address: Address, $T: typeid) -> T where size_of(T) == 1 {
	validate_memory_range(vm, address, size_of(T))
	return transmute(T)vm.memory[address]
}

load_16 :: #force_inline proc(vm: H16, address: Address, $T: typeid) -> T where size_of(T) == 2 {
	validate_memory_range(vm, address, size_of(T))
	a, b := vm.memory[address], vm.memory[address + 1]

	return transmute(T)(u16(a) | (u16(b) << 8))
}

load :: proc{load_8, load_16}

store_8 :: #force_inline proc(vm: ^H16, address: Address, val: $T) where size_of(T) == 1 {
	validate_memory_range(vm^, address, size_of(T))
	vm.memory[address] = transmute(u8)val
}

store_16 :: #force_inline proc(vm: ^H16, address: Address, val: $T) where size_of(T) == 2 {
	validate_memory_range(vm^, address, size_of(T))
	(transmute(^u16)&vm.memory[address])^ = transmute(u16)val
}

store :: proc{store_8, store_16}

// Debugging
h16_print_registers :: proc(vm: H16) {
	fmt.printf("pc: 0x%4.x sp: 0x%4.x bp: 0x%4.x cmp: 0b%3.b xsf: 0b%4.b xof: 0b%4.b gp: {}, {}, {}, {}\n",
		get_register(vm, .pc, Address),
		get_register(vm, .sp, Address),
		get_register(vm, .bp, Address),
		get_register(vm, .cmp, u16),
		get_register(vm, .xsf, u16),
		get_register(vm, .xof, u16),
		get_register(vm, .r1, i16),
		get_register(vm, .r2, i16),
		get_register(vm, .r3, i16),
		get_register(vm, .r4, i16))
}

h16_print_memory :: proc(vm: H16, start := 0, rows := 4) {
	// Col ofs
	fmt.printf("         ")
	for byte_ofs in 0..<16 {
		fmt.printf("0x%2.x ", byte_ofs)
	}
	fmt.println()

	// Vertical separator
	fmt.printf("       +")
	for byte_ofs in 0..<16 do fmt.printf("-----")
	fmt.println()

	// Values
	for row_ofs in 0..<rows {
		row := start + row_ofs
		row_start := row * 16

		fmt.printf("0x%4.x | ", row_start)

		for byte_ofs in 0..<16 {
			fmt.printf("0x%2.x ", vm.memory[row_start + byte_ofs])
		}

		fmt.println()
	}
}

h16_debug :: proc(vm: H16) {
	h16_print_registers(vm)
	h16_print_memory(vm)
}
