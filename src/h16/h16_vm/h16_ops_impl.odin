package h16_vm

import "core:fmt"
import "core:strings"
import "core:time"
import "core:slice"
import "core:math/bits"

import "../h16_types"
import "../h16_binary_iface"

@(private="file")
op_descriptors := &h16_types.op_descriptors

WRITE_OPS :: false
GATHER_TIMING :: false

when GATHER_TIMING {

	Op_Timing_Info :: struct {
		times_called: i64,
		time_total: time.Duration,
		op: Op,
	}

	op_infos: [Op]Op_Timing_Info

}

CMP_MASK_EQUAL   :: 1 << 0
CMP_MASK_GREATER :: 1 << 1
CMP_MASK_LESS    :: 1 << 2

// @todo: All comparisons are signed!
get_cmp_bits :: #force_inline proc "contextless" (a, b: i16) -> i16 {
	new_reg: i16

	if a == b do new_reg |= CMP_MASK_EQUAL
	if a < b  do new_reg |= CMP_MASK_LESS
	if a > b  do new_reg |= CMP_MASK_GREATER

	return new_reg
}

h16_run :: proc(vm: ^H16) {
	for #force_inline h16_run_single(vm) {}

	when GATHER_TIMING {
		_infos: [len(Op)]Op_Timing_Info; infos := _infos[:]
		for i in 0..<len(_infos) {
			_infos[i] = op_infos[Op(i)]
			_infos[i].op = Op(i)
		}

		slice.sort_by_cmp(infos, proc(a, b: Op_Timing_Info) -> slice.Ordering {
			return slice.Ordering(clamp(b.time_total - a.time_total, -1, 1))
		})

		for info, i in infos {
			if info.times_called == 0 do break

			avg := time.Duration(i64(info.time_total) / info.times_called)
			fmt.printf("#% 3.d: % 20.v | called: %- 10.d | total: %- 10.dus | avg: %- 5.dns\n",
				i,
				info.op,
				info.times_called,
				info.time_total / time.Microsecond,
				avg / time.Nanosecond)
		}
	}
}

h16_run_single :: proc(vm: ^H16) -> bool {
	pc := get_register(vm^, .pc, Address)
	op := load(vm^, pc, Op)
	vm.registers[.pc] += op_descriptors[op].size

	when WRITE_OPS {
		wr := 0
		wr += fmt.printf("0x%4.x | ", pc)

		di, ok := &vm.debug_info.?
		assert(ok)

		if source_loc, ok := di.source_mapping[u16(pc)]; ok {
			loc := h16_binary_iface.debug_info_temp_allocate_source_location(di, source_loc)
			source := h16_binary_iface.debug_info_fetch_source(di, source_loc)
			wr += fmt.printf("%-30s | ", loc)
			wr += fmt.printf("%-40s", source)
		} else {
			wr += fmt.printf("No symbols available")
		}
	}

	when GATHER_TIMING {
		start := time.tick_now()
	}

	switch op {
		case .Op_Hlt: {
			return false
		}

		case .Op_Dbg: {
			// h16_debug(vm^)
			h16_print_registers(vm^)
		}

		case .Op_Trap: {
			__trap()
		}

		case .Op_Jmp_Lbl: {
			addr := load(vm^, pc + 1, Address)
			set_register(vm, .pc, addr)
		}

		case .Op_Jmp_Reg: {
			reg := load(vm^, pc + 1, Register)
			addr := get_register(vm^, reg, Address)

			set_register(vm, .pc, addr)
		}

		case .Op_Cmp_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			val1 := get_register(vm^, reg1, i16)
			val2 := get_register(vm^, reg2, i16)

			set_register(vm, .cmp, get_cmp_bits(val1, val2))
		}

		case .Op_Cmp_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, i16)

			val := get_register(vm^, reg, i16)

			set_register(vm, .cmp, get_cmp_bits(val, lit))
		}

		case .Op_Cmp_Lit_Reg: {
			lit := load(vm^, pc + 1, i16)
			reg := load(vm^, pc + 3, Register)

			val := get_register(vm^, reg, i16)

			set_register(vm, .cmp, get_cmp_bits(lit, val))
		}

		case .Op_Je_Lbl: {
			cmp_bits := get_register(vm^, .cmp, i16)

			if (cmp_bits & CMP_MASK_EQUAL) != 0 {
				set_register(vm, .pc, load(vm^, pc + 1, Address))
			}
		}

		case .Op_Jne_Lbl: {
			cmp_bits := get_register(vm^, .cmp, i16)

			if (cmp_bits & CMP_MASK_EQUAL) == 0 {
				set_register(vm, .pc, load(vm^, pc + 1, Address))
			}
		}

		case .Op_Jl_Lbl: {
			cmp_bits := get_register(vm^, .cmp, i16)

			if (cmp_bits & CMP_MASK_LESS) != 0 {
				set_register(vm, .pc, load(vm^, pc + 1, Address))
			}
		}

		case .Op_Jle_Lbl: {
			cmp_bits := get_register(vm^, .cmp, i16)

			if (cmp_bits & (CMP_MASK_LESS | CMP_MASK_EQUAL)) != 0 {
				set_register(vm, .pc, load(vm^, pc + 1, Address))
			}
		}

		case .Op_Jg_Lbl: {
			cmp_bits := get_register(vm^, .cmp, i16)

			if (cmp_bits & CMP_MASK_GREATER) != 0 {
				set_register(vm, .pc, load(vm^, pc + 1, Address))
			}
		}

		case .Op_Jge_Lbl: {
			cmp_bits := get_register(vm^, .cmp, i16)

			if (cmp_bits & (CMP_MASK_GREATER | CMP_MASK_EQUAL)) != 0 {
				set_register(vm, .pc, load(vm^, pc + 1, Address))
			}
		}

		case .Op_Callext_Lit_Lit: {
			lit1 := load(vm^, pc + 1, i16)
			lit2 := load(vm^, pc + 3, i16)

			vm.syscall(vm, lit1, lit2)
		}

		case .Op_Callext_Lit_Reg: {
			lit := load(vm^, pc + 1, i16)
			reg := load(vm^, pc + 3, Register)

			vm.syscall(vm, lit, get_register(vm^, reg, i16))
		}

		case .Op_Callext_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			vm.syscall(vm, get_register(vm^, reg1, i16), get_register(vm^, reg2, i16))
		}

		case .Op_Mov_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, i16)

			assert_register_writable(reg)

			set_register(vm, reg, lit)
		}

		case .Op_Mov_Adr_Reg: {
			addr := load(vm^, pc + 1, Address)
			reg := load(vm^, pc + 3, Register)

			store(vm, addr, get_register(vm^, reg, i16))
		}

		case .Op_Mov_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg2, i16))
		}

		case .Op_Inc_Reg: {
			reg := load(vm^, pc + 1, Register)

			assert_register_writable(reg)

			inc_register(vm, reg, 1)
		}

		case .Op_Dec_Reg: {
			reg := load(vm^, pc + 1, Register)

			assert_register_writable(reg)

			inc_register(vm, reg, -1)
		}

		case .Op_Add_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, i16) + get_register(vm^, reg2, i16))
		}

		case .Op_Add_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, i16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, i16) + lit)
		}

		case .Op_Sub_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, i16) - get_register(vm^, reg2, i16))
		}

		case .Op_Sub_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, i16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, i16) - lit)
		}

		case .Op_Mul_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, i16) * get_register(vm^, reg2, i16))
		}

		case .Op_Mul_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, i16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, i16) * lit)
		}

		case .Op_Div_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) / get_register(vm^, reg2, u16))
		}

		case .Op_Div_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) / lit)
		}

		case .Op_Mod_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) % get_register(vm^, reg2, u16))
		}

		case .Op_Mod_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) % lit)
		}

		case .Op_And_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) & get_register(vm^, reg2, u16))
		}

		case .Op_And_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) & lit)
		}

		case .Op_Or_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) | get_register(vm^, reg2, u16))
		}

		case .Op_Or_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) | lit)
		}

		case .Op_Not_Reg: {
			reg := load(vm^, pc + 1, Register)

			assert_register_writable(reg)

			set_register(vm, reg, ~get_register(vm^, reg, u16))
		}

		case .Op_Xor_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) ~ get_register(vm^, reg2, u16))
		}

		case .Op_Xor_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) ~ lit)
		}

		case .Op_Shl_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) << get_register(vm^, reg2, u16))
		}

		case .Op_Shl_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) << lit)
		}

		case .Op_Rol_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, bits.rotate_left16(get_register(vm^, reg1, u16), int(get_register(vm^, reg2, u16))))
		}

		case .Op_Rol_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, bits.rotate_left16(get_register(vm^, reg, u16), int(lit)))
		}

		case .Op_Sar_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)

			set_register(vm, reg1, get_register(vm^, reg1, u16) >> get_register(vm^, reg2, u16))
		}

		case .Op_Sar_Reg_Lit: {
			reg := load(vm^, pc + 1, Register)
			lit := load(vm^, pc + 2, u16)

			assert_register_writable(reg)

			set_register(vm, reg, get_register(vm^, reg, u16) >> lit)
		}

		case .Op_Call_Lbl: {
			addr := load(vm^, pc + 1, Address)

			pc := get_register(vm^, .pc, Address)
			sp := get_register(vm^, .sp, Address)

			store(vm, sp, pc)
			inc_register(vm, .sp, -2)

			set_register(vm, .pc, addr)
		}

		case .Op_Call_Reg: {
			reg := load(vm^, pc + 1, Register)
			val := get_register(vm^, reg, Address)

			pc := get_register(vm^, .pc, Address)
			sp := get_register(vm^, .sp, Address)

			store(vm, sp, pc)
			inc_register(vm, .sp, -2)

			set_register(vm, .pc, val)
		}

		case .Op_Ret: {
			inc_register(vm, .sp, 2)
			sp := get_register(vm^, .sp, Address)
			val := load(vm^, sp, Address)

			set_register(vm, .pc, val)
		}

		case .Op_Push_Reg: {
			reg := load(vm^, pc + 1, Register)
			val := get_register(vm^, reg, i16)
			sp := get_register(vm^, .sp, Address)

			store(vm, sp, val)
			inc_register(vm, .sp, -2)
		}

		case .Op_Push_Lit: {
			lit := load(vm^, pc + 1, i16)
			sp := get_register(vm^, .sp, Address)

			store(vm, sp, lit)
			inc_register(vm, .sp, -2)
		}

		case .Op_Pop_Reg: {
			reg := load(vm^, pc + 1, Register)
			inc_register(vm, .sp, 2)
			sp := get_register(vm^, .sp, Address)

			assert_register_writable(reg)

			set_register(vm, reg, load(vm^, sp, i16))
		}

		case .Op_Pop: {
			inc_register(vm, .sp, 2)
		}

		case .Op_Xchg_Reg_Reg: {
			reg1 := load(vm^, pc + 1, Register)
			reg2 := load(vm^, pc + 2, Register)

			assert_register_writable(reg1)
			assert_register_writable(reg2)

			val1 := get_register(vm^, reg1, i16)
			val2 := get_register(vm^, reg2, i16)

			set_register(vm, reg1, val2)
			set_register(vm, reg2, val1)
		}

		case: unimplemented()
	}

	when GATHER_TIMING {
		time_took := time.tick_diff(start, time.tick_now())
		op_infos[op].times_called += 1
		op_infos[op].time_total += time_took
	}

	when WRITE_OPS do fmt.printf(strings.repeat(" ", max(0, 40 - wr), context.temp_allocator))
	when WRITE_OPS do fmt.printf("| ")
	when WRITE_OPS do h16_print_registers(vm^)

	return true;
}
