// This package represents the interface between the VM and any generator of
// bytecode for the VM.
package h16_binary_iface

import "core:fmt"

// The first and only required input to a VM. This data is simply copied into
// the VMs memory.
// The program counter begins at 0.
H16_Bytecode :: distinct []u8

// @todo: This should be (de)serializable! This means maybe not using [dynamic],
// and finding a way to preallocate maps.

// The second part that can be passed to a VM. It is optional.
// This info, if debug is enabled, will be used to show nicer output while the
// VM is running.
Debug_Info :: struct {
	// Used to keep source data alive.
	source_files: [dynamic]Source_File,

	// Mapping from offset in binary of instruction to range in source file.
	source_mapping: map[u16]Source_Location,
}

Source_Location :: struct {
	// offset could be derived from line + column + file, but I've opted to
	// add this to make it easier at runtime.
	start, end: struct { line, column, offset: int },
	// Index into debug_info.source_files
	file_index: int,
}

Source_File :: struct {
	filename: string,
	data: []byte,
}

// Appends source file and returns file index for use in Source_Location.
// Note that source file must be heap allocated, and that you should not
// free it yourself!
debug_info_append_source_file :: proc(debug_info: ^Debug_Info, source_file: Source_File) -> int {
	append(&debug_info.source_files, source_file)
	return len(debug_info.source_files) - 1
}

// Fetches allocated source info
debug_info_fetch_source :: proc(debug_info: ^Debug_Info, source_location: Source_Location) -> string {
	file := debug_info.source_files[source_location.file_index]
	return string(file.data[source_location.start.offset:source_location.end.offset])
}

debug_info_temp_allocate_source_location :: proc(
	debug_info: ^Debug_Info,
	source_location: Source_Location,
) -> string {
	filename := debug_info.source_files[source_location.file_index].filename

	return fmt.tprintf("{}:{}:{}",
		filename,
		source_location.start.line,
		source_location.start.column)
}

debug_info_free :: proc(debug_info: ^Debug_Info) {
	for source_file in &debug_info.source_files {
		delete(source_file.filename)
		delete(source_file.data)
	}

	delete(debug_info.source_mapping)
}
