package h16_assembler

import "../h16_types"
import "../h16_binary_iface"

Op :: h16_types.Op
Register :: h16_types.Register
Address :: h16_types.Address
Param_Type :: h16_types.Param_Type

import "core:os"
import "core:fmt"
import "core:strconv"
import "core:strings"
import "core:math/bits"
import "core:reflect"

@(private="file")
op_descriptors := &h16_types.op_descriptors

@(private="file")
is_whitespace :: #force_inline proc(c: byte) -> bool {
	return c == ' ' || c == '\t'
}

@(private="file")
is_word :: #force_inline proc(c: byte) -> bool {
	return !is_whitespace(c) && c != ';' && c != '\n'
}

@(private="file")
Parse_Context :: struct {
	file_index: int,
	line_source: string,
	line_number: int,
}

// @todo: Assembler kinda sucks:
// - Unsafe usage of temp allocator: sometimes allocates by length of line
// - Not structured well, there are certainly a lot of edge cases that work weirdly
// - No validation that words/registers etc are well formed - make sure that they only consist of a group of letters?

assemble_from_file :: proc(entry_file: string) -> (bin: h16_binary_iface.H16_Bytecode, debug_info: h16_binary_iface.Debug_Info) {
	// map from filename to processed
	// @todo: Make sure filenames are fully qualified
	files := make(map[string]bool)
	defer delete(files)
	files[entry_file] = false

	builder := builder_create()

	for {
		filename: string
		for file, processed in files {
			if processed do continue

			filename = file
		}

		if filename == "" do break

		data, ok := os.read_entire_file(filename)
		if !ok {
			fmt.panicf("Could not open file '{}'!\n", filename)
		}

		file_index := h16_binary_iface.debug_info_append_source_file(
			&builder.debug_info,
			h16_binary_iface.Source_File { strings.clone(filename), data })

		ci := 0
		line_number := 0

		for ci < len(data) {
			line_start := ci

			for data[ci] != '\n' && data[ci] != '\r' && ci < len(data) {
				ci += 1
			}

			entire_line := string(data[line_start:ci])
			trimmed_line := strings.trim(entire_line, "\t ")

			pc := Parse_Context {
				file_index = file_index,
				line_number = line_number,
				line_source = entire_line,
			}
			process_line(trimmed_line, &builder, &files, pc, line_start)
			line_number += 1

			ci += 1
		}

		files[filename] = true
	}

	// Now insert jump targets
	for offset, label in builder.jump_targets {
		if label_addr, ok := builder.labels[label]; ok {
			builder.data[offset+0] = u8(label_addr & 0xff)
			builder.data[offset+1] = u8((label_addr >> 8) & 0xff)
		} else {
			fmt.panicf("Label '{}' not found!\n", label)
		}
	}

	// Insert constant values
	for offset, name in builder.constant_references {
		if constant, ok := builder.constants[name]; ok {
			builder.data[offset+0] = u8(constant & 0xff)
			builder.data[offset+1] = u8((constant >> 8) & 0xff)
		} else {
			fmt.panicf("Constant '{}' not found!\n", name)
		}
	}

	return builder_finalize(builder)
}

// @todo: Make optimized version without allocations
split_whitespace_consecutive :: proc(
	str: string,
	allocator := context.temp_allocator,
) -> []string {
	words := make([dynamic]string, 0, 8, allocator)
	for ci := 0; ci < len(str); {
		// Chomp word
		word_start := ci
		for ci < len(str) && str[ci] != ' ' do ci += 1
		append(&words, str[word_start:ci])
		for ci < len(str) && str[ci] == ' ' do ci += 1
	}

	return words[:]
}

// Takes a string, converts to int, validates that it fits in 16 bits, and handles sign.
// Note that the return value will always be i16, though internally it may be a transmuted u16.
@(private="file")
parse_int_string :: proc(str: string) -> i16 {
	val, ok := strconv.parse_int(str)
	if !ok {
		fmt.panicf("Couldn't parse int '{}'\n", str)
	}

	if val < 0 {
		assert(val >= bits.I16_MIN)
		return i16(val)
	} else {
		assert(val <= bits.U16_MAX)
		return transmute(i16)u16(val)
	}
}

is_probably_number :: proc(str: string) -> bool {
	assert(len(str) > 0)
	switch str[0] {
	// This is *extremely* robust
	case '0'..='9': return true
	case: return false
	}
}

is_uppercase :: proc(str: string) -> bool {
	for c in str do switch c {
	case 'A'..='Z', '_':
	case: return false
	}

	return true
}

process_line :: proc(
	line: string,
	b: ^Builder,
	files: ^map[string]bool,
	pc: Parse_Context,
	line_offset: int,
) {
	if line == "" do return

	// Check if there's a commented out part of the line
	for ci in 0..<len(line) {
		if line[ci] == ';' {
			process_line(line[:ci], b, files, pc, line_offset)

			return
		}
	}

	if line[0] == '@' {
		split := strings.split_n(line[1:], " ", 2, context.temp_allocator)
		directive := split[0]
		remainder := split[1]

		switch directive {
			case "include": {

				if !(remainder in files) {
					files[remainder] = false
				}
			}

			case "const": {
				key_value := strings.split(remainder, " ", context.temp_allocator)
				if len(key_value) != 2 {
					fmt.panicf("Invalid const on line '{}'\n", line)
				}

				key := key_value[0]
				assert(is_uppercase(key))

				if key in b.constants {
					fmt.panicf("Constant {} declared multiple times!", key)
				}

				value := parse_int_string(key_value[1])

				b.constants[key] = value
			}

			case: {
				fmt.panicf("Unrecognized directive in line '{}'\n", line)
			}
		}

		return
	}

	// Check if line contains label
	for ci in 0..<len(line) {
		c := line[ci]

		// Leading whitespace is trimmed, so space means that we're after the first word.
		// Labels are no longer valid here
		if c == ' ' {
			assert(!strings.contains(line, ":"))
		}

		if line[ci] == ':' {
			builder_insert_label(b, line[:ci])
			// We process lines recursively. This means that multiple labels per line is allowed
			process_line(line[ci+1:], b, files, pc, line_offset + ci + 1)

			return
		}
	}

	args := split_whitespace_consecutive(line)

	if len(args) == 0 {
		return
	}

	first := args[0]

	if first[len(first)-1:] == ":" {
		builder_insert_label(b, first[:len(first)-1])
		return
	}

	mneumonic := first
	params := args[1:]

	any_match := false

	for descriptor, op in op_descriptors {
		if descriptor.mneumonic != mneumonic do continue
		if descriptor.param_count != len(params) do continue

		match := true
		for param_idx := 0; param_idx < descriptor.param_count; param_idx += 1 {
			param := descriptor.params[param_idx]
			if param != word_into_param_type(params[param_idx]) {
				match = false
				break
			}
		}

		if match {
			any_match = true
			// Since this is just an assembler, we cheat a bit and just say that the whole
			// line is the source for this op. We don't actually even know where within the line we are.
			b.debug_info.source_mapping[u16(len(b.data))] = h16_binary_iface.Source_Location {
				// We fill in the offsets after the fact
				start = { line = pc.line_number, column = 0, offset = line_offset },
				end = { line = pc.line_number, column = len(pc.line_source), offset = line_offset + len(pc.line_source) },
				file_index = pc.file_index,
			}
			builder_write_op(b, op)

			for param_idx := 0; param_idx < descriptor.param_count; param_idx += 1 {
				switch descriptor.params[param_idx] {
				case .Literal: {
					// A literal can be either a literal, or a constant. We figure out which it is first
					param := params[param_idx]
					if is_probably_number(param) {
						// Hmmm.. good enough.
						builder_write_i16(b, parse_int_string(params[param_idx]))
					} else {
						// Well we might not know the constant value yet. So save a reference and solve it later.
						b.constant_references[len(b.data)] = param
						builder_write_u16(b, 0xabcd) // Some garbage for now
					}
				}
				case .Register: builder_write_reg(b, word_into_register(params[param_idx]))
				case .Address: builder_write_u16(b, u16(word_into_address(params[param_idx])))
				case .Label: {
					// Label might not be available now - insert 0xea60, and make a note to come back
					// and insert real address
					b.jump_targets[len(b.data)] = word_into_label_string(params[param_idx])
					builder_write_u16(b, 0xea60)
				}
				case .Invalid: panic("Trying to emit invalid param!")
				}
			}

			break
		}
	}

	if !any_match {
		fmt.panicf("Assembler no match! {}\n", args)
	}
}

// Sucks but works for now
@(private="file")
word_into_param_type :: proc(word: string) -> Param_Type {
	switch word[:1] {
		case "%": return .Register
		case "#": return .Label
		case "&": return .Address
		case: return .Literal
	}
}

// @todo: Remove if/when register names become the same as in source
@(private="file")
word_into_register :: proc(word: string) -> Register {
	assert(len(word) > 1)
	word_lower := strings.to_lower(word[1:], context.temp_allocator)
	for field in reflect.enum_fields_zipped(Register) {
		if field.name == word_lower do return Register(field.value)
	}

	fmt.panicf("Unrecognized register '{}'\n", word)
}

// @todo: Better validation
@(private="file")
word_into_address :: proc(word: string) -> Address {
	return Address(strconv.atoi(word[1:]))
}

@(private="file")
word_into_label_string :: proc(word: string) -> string {
	return word[1:]
}
