package h16_assembler

import "core:os"
import "core:strings"
import "core:fmt"

import "../h16_binary_iface"
import "../h16_types"

H16_Bytecode :: h16_binary_iface.H16_Bytecode
Debug_Info   :: h16_binary_iface.Debug_Info

@(private="file")
Builder_Intermediate :: struct {
	// Map from label name to code offset
	labels: map[string]int,

	// Map from jump location to target label
	jump_targets: map[int]string,

	// Map from constant name to constant value
	constants: map[string]i16,

	// Map from constant location to target constant
	constant_references: map[int]string,
}

@(private="file")
builder_free_intermediate :: proc(intermediate: Builder_Intermediate) {
	delete(intermediate.labels)
	delete(intermediate.jump_targets)
	delete(intermediate.constants)
	delete(intermediate.constant_references)
}

Builder :: struct {
	data: [dynamic]u8,
	// Used while building, but can also be returned for use in a vm
	using debug_info: Debug_Info,
	using intermedite_info: Builder_Intermediate,
}

builder_create :: proc() -> Builder {
	builder: Builder
	builder.debug_info = h16_binary_iface.Debug_Info {}

	return builder
}

builder_finalize :: proc(builder: Builder) -> (data: H16_Bytecode, debug_info: Debug_Info) {
	builder_free_intermediate(builder.intermedite_info)
	return H16_Bytecode(builder.data[:]), builder.debug_info
}

builder_write_op :: proc(builder: ^Builder, op: Op) {
	append(&builder.data, u8(op))
}

builder_write_reg :: proc(builder: ^Builder, reg: Register) {
	append(&builder.data, u8(reg))
}

builder_write_i16 :: proc(builder: ^Builder, val: i16) {
	val := transmute(u16)val
	append(&builder.data, u8(val & 0xff))
	append(&builder.data, u8((val >> 8) & 0xff))
}

builder_write_u16 :: proc(builder: ^Builder, val: u16) {
	append(&builder.data, u8(val & 0xff))
	append(&builder.data, u8((val >> 8) & 0xff))
}

builder_insert_label :: proc(builder: ^Builder, label: string) {
	// @todo: Better validation
	assert(label != "" && !strings.contains(label, ":%&;"), "Invalid label!")

	if label in builder.labels {
		fmt.panicf("Label '{}' already exists!\n", label)
	}

	builder.labels[label] = len(builder.data)
}

builder_write_label_addr :: proc(builder: ^Builder, label: string) {
	if loc, ok := builder.labels[label]; ok {
		builder_write_u16(builder, u16(loc))
	} else {
		assert(false, "Label not found!")
	}
}
