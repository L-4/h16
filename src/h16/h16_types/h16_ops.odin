package h16_types

import "core:strings"
import "core:reflect"
import "core:fmt"

Address :: distinct u16

Op :: enum u8 {
	// @todo: Good idea? "out of instructions" halt would serve as a hint that you're
	// running zeroed memory.
	// Op_Ooi,
	Op_Hlt,
	Op_Dbg,
	Op_Trap,

	Op_Jmp_Lbl,
	Op_Jmp_Reg,

	Op_Cmp_Reg_Reg,
	Op_Cmp_Reg_Lit,
	Op_Cmp_Lit_Reg,

	Op_Je_Lbl,
	Op_Jne_Lbl,
	Op_Jl_Lbl,
	Op_Jle_Lbl,
	Op_Jg_Lbl,
	Op_Jge_Lbl,

	Op_Callext_Lit_Lit,
	Op_Callext_Lit_Reg,
	Op_Callext_Reg_Reg,

	Op_Mov_Reg_Lit,
	Op_Mov_Adr_Reg,
	Op_Mov_Reg_Reg,

	Op_Inc_Reg,
	Op_Dec_Reg,

	// @todo: Include versions with memory addresses?
	Op_Add_Reg_Reg,
	Op_Add_Reg_Lit,
	Op_Sub_Reg_Reg,
	Op_Sub_Reg_Lit,
	Op_Mul_Reg_Reg,
	Op_Mul_Reg_Lit,
	Op_Div_Reg_Reg,
	Op_Div_Reg_Lit,
	Op_Mod_Reg_Reg, // @todo: Sign?
	Op_Mod_Reg_Lit,

	Op_And_Reg_Reg,
	Op_And_Reg_Lit,
	Op_Or_Reg_Reg,
	Op_Or_Reg_Lit,
	Op_Not_Reg,
	Op_Xor_Reg_Reg,
	Op_Xor_Reg_Lit,

	Op_Shl_Reg_Reg,
	Op_Shl_Reg_Lit,
	Op_Rol_Reg_Reg,
	Op_Rol_Reg_Lit,
	// Op_Shr_Reg_Reg,
	// Op_Shr_Reg_Lit,
	Op_Sar_Reg_Reg,
	Op_Sar_Reg_Lit,
	// Op_Ror_Reg_Reg,
	// Op_Ror_Reg_Lit,

	Op_Call_Lbl,
	Op_Call_Reg,

	Op_Ret,

	Op_Push_Reg,
	Op_Push_Lit,

	Op_Pop_Reg,
	Op_Pop,

	Op_Xchg_Reg_Reg,
}

Op_Descriptor :: struct {
	size: i16,
	mneumonic: string,
	params: [4]Param_Type,
	param_count: int,
}

op_descriptors: [Op]Op_Descriptor

Param_Type :: enum { Literal, Register, Address, Label, Invalid }
param_sizes := [Param_Type]i16 {
	.Literal = 2,
	.Register = 1,
	.Address = 2,
	.Label = 2, // Becomes address
	.Invalid = -1,
}

word_into_param_type :: proc(word: string) -> Param_Type {
	switch word {
		case "Lbl": return .Label
		case "Lit": return .Literal
		case "Reg": return .Register
		case "Adr": return .Address
		case: fmt.panicf("Unrecognized param type '{}'\n", word)
	}
}

word_into_mneumonic :: proc(word: string) -> string {
	// This sucks, but means that only static allications are required, as all
	// fields will point back at these strings
	switch word {
		case "Hlt": return "hlt"
		case "Dbg": return "dbg"
		case "Trap": return "trap"
		case "Call": return "call"
		case "Ret": return "ret"
		case "Callext": return "callext"
		case "Jmp": return "jmp"
		case "Mov": return "mov"
		case "Inc": return "inc"
		case "Dec": return "dec"
		case "Add": return "add"
		case "Sub": return "sub"
		case "Mul": return "mul"
		case "Div": return "div"
		case "Mod": return "mod"
		case "And": return "and"
		case "Not": return "not"
		case "Or": return "or"
		case "Xor": return "xor"
		case "Shl": return "shl"
		case "Rol": return "rol"
		case "Shr": return "shr"
		case "Ror": return "ror"
		case "Sar": return "Sar"
		case "Je": return "je"
		case "Jne": return "jne"
		case "Jl": return "jl"
		case "Jle": return "jle"
		case "Jg": return "jg"
		case "Jge": return "jge"
		case "Cmp": return "cmp"
		case "Push": return "push"
		case "Pop": return "pop"
		case "Xchg": return "xchg"
		case: fmt.panicf("Invalid mneumonic: '{}'\n", word)
	}
}

generate_op_descriptors :: proc() {
	for field in reflect.enum_fields_zipped(Op) {
		op_base := field.name[3:]
		op_fields := strings.split(op_base, "_", context.temp_allocator)
		assert(len(op_fields) > 0)

		mneumonic := op_fields[0]
		params := op_fields[1:]

		descriptor := Op_Descriptor {
			size = 1, // Op itself takes one byte
			mneumonic = word_into_mneumonic(mneumonic),
			params = { .Invalid, .Invalid, .Invalid, .Invalid },
			param_count = len(params),
		}

		for field, i in params {
			param_type := word_into_param_type(field)
			descriptor.params[i] = param_type
			descriptor.size += param_sizes[param_type]
		}

		assert(int(field.value) <= int(max(Op)))
		op_descriptors[Op(field.value)] = descriptor
	}
}

Register_Bits :: bit_set[Register]
Register :: enum u8 {
	pc, sp, bp, cmp,
	r1, r2, r3, r4,
	xsf, xof, // External status/operation flags
}

writable_registers := Register_Bits{.bp, .r1, .r2, .r3, .r4, .xof}
